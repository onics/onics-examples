topflows
=====

This repo contains some sample shell scripts for monitoring local traffic
using ONICS and looking at the top 20 flows sorted by total traffic volume.
The main script is topflows.sh.  You run it as:

    # ./topflows.sh IFNAME

where IFNAME is the name of the interface to watch traffic on.  You can
also run it using sudo instead of root.


If you'd like to test what the output will look like you can run the
subscripts with the following command:

    $ cat sample-flows |
        sort -s -t '|' -k 3,3 | 
        awk -f uniqflows.awk | 
        awk -f prflow.awk | 
        sort -s -t ',' -k 3 -r |
        awk -f columns.awk 

This should give output similar (whitespace trimmed) to:

    Flow                                               Duration      Bytes
    tcp//192.168.0.4:tcp<->192.168.0.7:ssh             0.0010         9685
    tcp//192.168.0.4:tcp<->ok-in-f147.1e100.net:http   0.0000         7499
    udp//192.168.0.7:ipp<->192.168.0.255:ipp           0.0010          660
    icmp//192.168.0.4<->192.168.0.1                    0.0010          392
    udp//192.168.0.4:udp<->192.168.0.1:domain          0.0000          336
    udp//192.168.0.4:udp<->192.168.0.1:domain          0.0000          218
    arp//192.168.0.4<->192.168.0.7                     0.0010          102
    igmp//192.168.0.254<->all-systems.mcast.net        0.0010           84
    igmp//192.168.0.43<->224.0.0.251                   0.0010           60
    igmp//192.168.0.4<->224.0.0.251                    0.0010           46


In order to run these scripts you need the following installed:
  * ONICS
  * various \*NIX command line tools including awk, sed, sort, etc..
  * dig (for DNS lookups)

I've run this on my OpenBSD firewall at home.
