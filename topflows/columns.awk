BEGIN { 
	FS = ","
	printf("%-52s  %8s  %14s\n", "Flow", "Duration", "Bytes")
}

{ printf("%-52s  %8.4f  % 14d\n", $1, $2, $3 + 0) }
   
