function gethost(flowstr, tag) {
	addr = ""
	if (flowstr ~ /(IP:|ARP:|IP6:|ETH:)/) {
		pfxlen = length(tag)+1
		match(flowstr, tag "=[0-9a-fA-F.:]*")
		addr = substr(flowstr, RSTART+pfxlen, RLENGTH-pfxlen)
		if (flowstr !~ /ETH:/) {
			rcmd = "./resolve.sh " addr
			rcmd | getline raddr
			close(rcmd)
			addr = raddr
		}
	}
	return addr
}

function toname(str, file) {
	cmd = "awk '$2 == \"" str "\" {print $1; exit 0}' " file
	cmd | getline name
	close(cmd)
	if (name)
		return tolower(name)
	else
		return str
}

BEGIN { 
	FS="|" 
#	printf("%-52s  %8s  %14s\n", "Flow", "Duration", "Bytes");
}

{
    # Extract the flow duration if known
    dur = 0.0;
    if ($2 !~ /START/) {
        dur=$4
        sub("^.*Dur=", "", dur);
    }

    # Count total number of bytes from each direction
    if ($5 ~ /SENT/) {
        bytes=$5
        sub("SENT:[0-9]*,", "", bytes)
    } else {
        c2s=$5
        s2c=$6
        sub("C2S:[0-9]*,", "", c2s)
        sub("S2C:[0-9]*,", "", s2c)
        bytes = c2s + s2c
    }

    # Extract the source and destination address if present
    ca = gethost($3, "ca")
    sa = gethost($3, "sa")

    # Attempt to find the IP protocol
    if ($3 ~ /(IP:|IP6:).*proto=/) {
        match($3, "proto=[0-9]*")
	proto = substr($3, RSTART+6, RLENGTH-6)
        proto = toname(proto, "/etc/protocols")
    } else {
        proto = tolower($3)
        sub(":.*", "", proto)
    }

    # Extract the source and destination port if present
    cpt = ""
    spt = ""
    if ($3 ~ /cpt=/) {
        match($3, "cpt=[0-9]*")
        cpt = substr($3, RSTART+4, RLENGTH-4) 
        cpt = ":" toname(cpt "/" proto, "/etc/services")
        match($3, "spt=[0-9]*")
        spt = substr($3, RSTART+4, RLENGTH-4)
        spt = ":" toname(spt "/" proto, "/etc/services")
    }

    flow = sprintf("%s//%s%s<->%s%s", proto, ca, cpt, sa, spt);
    printf("%s,%.4f,%014d\n", flow, dur, bytes);
}
