#!/bin/sh

pktin $1 | nftrk -d -f /tmp/flows.txt & 
PID=$!

trap "kill $PID ; rm -f /tmp/flows.txt /tmp/topflows.txt /tmp/namecache.txt ; exit 0" INT TERM

while [ 1 ] ; do
    tail -100 /tmp/flows.txt |
            sort -s -t '|' -k 3,3 |
            awk -f uniqflows.awk  |
            awk -f prflow.awk  |
            sort -s -t ',' -k 3 -r |
            awk -f columns.awk |
            head -20 > /tmp/topflows.txt
    clear
    cat /tmp/topflows.txt
    sleep 1
done

