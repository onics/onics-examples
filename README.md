ONICS Examples
=====

This repository contains programs (scripts or otherwise) that are sample
uses of the ONICS tool suite.  Each subdirectory should have its own
README that indicates hwo to make use of the code therein.  The programs
in this repo are not intended to be maintained at full production
quality.  That doesn't mean the code won't be useful or will be shoddy.
But they will not be maintained with the same rigor as ONICS.
