#!/bin/sh

if [ ! -f /tmp/namecache.txt ] ; then
	sed -e '/^[ 	]*#/ d' -e '/^[ 	]*$/ d' /etc/hosts | 
		awk '{print $1, $2}' | sort | uniq > /tmp/namecache.txt
fi


if grep -F -q "$1 " /tmp/namecache.txt
then
	NAME=`grep -F "$1 " /tmp/namecache.txt | awk '{print $2}'`
else
	ANS=`dig +noall +answer -x $1`
	if [ "$ANS" = "" ] ; then
		NAME=$1
	else
		NAME=`echo $ANS | sed -e 's/^.*PTR *//' -e 's/\.$//'`
	fi
	echo "$1 $NAME" >> /tmp/namecache.txt
fi
echo $NAME
